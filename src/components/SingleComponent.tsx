import {Box, Button, Flex, HStack, IconButton, Input, Spacer, Tag, Text, useToast} from "@chakra-ui/react";
import {ComponentState} from "../types/propTypes";
import React, {useCallback, useRef, useState} from "react";
import {FaLock, FaUnlock} from "react-icons/fa";

interface ISingleComponentProps {
    item: ComponentState
    onChange: (comp: ComponentState) => void
}

export default function SingleComponent(
    {
        item,
        onChange
    }: ISingleComponentProps) {
    const toast = useToast();
    const inputRef = useRef<HTMLInputElement>(null);
    const editInputRef = useRef<HTMLInputElement>(null);
    const [isEditing, setIsEditing] = useState<boolean>(false);
    const [locks, setLocks] = useState<any>({})

    const onEditSubmit = useCallback((e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            onChange({...item, name: editInputRef.current?.value ?? "Generic Name"})
            setIsEditing(false);
        }
    }, [item, onChange])

    const onTagClick = useCallback(() => {
        setIsEditing(true);
        setTimeout(() => {
            editInputRef.current?.focus();
        }, 50)
    }, [])

    const onAdjust = useCallback((e: React.ChangeEvent<HTMLInputElement>, key) => {
        const num = Number.parseFloat(e.target.value);
        if (num < 100 || Number.isNaN(num)) {
            const newWeights = [...item.weights];
            newWeights[key] = num;
            if (!Number.isNaN(num)) {


                let sumLocks = 0;
                let unLockCount = 0;
                newWeights.forEach((val, k) => {
                    if (locks[k] || key === k) {
                        sumLocks += val
                    } else {
                        unLockCount++;
                    }
                })

                for (let i = 0; i < newWeights.length; i++) {
                    if (!locks[i] && i !== key) {
                        const num = Math.round(((100 - sumLocks) / unLockCount) * 100) / 100;
                        if (num < 0) {
                            toast.closeAll();
                            return toast({
                                title: "Negative overflow: cannot accept " + num,
                                status: "error",

                            })
                        }
                        newWeights[i] = Math.round(((100 - sumLocks) / unLockCount) * 100) / 100;
                    }
                }
            }
            onChange({...item, weights: newWeights})

        }
    }, [item, onChange, locks, toast])

    const onAdjustLocks = useCallback((key: number) => {
        setLocks({...locks, [key]: !locks[key]})
    }, [locks])

    const onCreate = useCallback((e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {

            const num = Number.parseInt(inputRef.current?.value ?? "0");
            const arr = new Array(num).fill(Math.floor(100 / num * 100) / 100)

            setLocks({})
            onChange({
                ...item,
                count: num,
                weights: arr,
            })
        }
    }, [item, onChange])

    const toggleLocks = () => {
        const l = {...locks};
        for (let i = 0; i < item.weights.length; i++) {
            l[i] = true;

        }
        setLocks(l)
    }

    return (
        <Box>
            <Flex alignItems={"center"}>
                <Flex alignItems={"center"}>
                    {
                        isEditing ?
                            <Input
                                mr={1}
                                size={"sm"}
                                ref={editInputRef}
                                defaultValue={item.name}
                                onKeyDown={onEditSubmit}
                            />
                            :
                            <HStack spacing={1}>
                                <Tag onClick={onTagClick} mr={'2'}>{item.name}</Tag>
                                {
                                    (() => {
                                        const sum = item.weights.reduce((prev, curr) => prev + curr, 0)
                                        return <Tag whiteSpace={"nowrap"} onClick={onTagClick}
                                                    colorScheme={Math.floor(sum) > 100 ? "red" : "green"}
                                                    mr={'2'}>Sum: {Math.floor(sum)}</Tag>
                                    })()
                                }
                                <IconButton onClick={toggleLocks} size={"xs"} aria-label={"lock-all"} icon={<FaLock/>}/>
                            </HStack>
                    }
                    <Input ml={1} ref={inputRef} type={"number"} size={"xs"} w={"50px"} defaultValue={item.count}
                           onKeyDown={onCreate}/>
                </Flex>
                <Spacer/>
                <HStack overflowX={"auto"} ml={8} width={"100%"}>
                    {
                        item.weights.map((value, key) => (
                            <HStack height={12} key={key}>
                                <Text>{key + 1}:</Text>
                                <Input disabled={locks[key]} minWidth={"60px"} size={"xs"} type={'number'} max={100} value={value}
                                       onChange={(e) => onAdjust(e, key)}/>
                                <Button colorScheme={locks[key] ? "red" : "green"} onClick={() => onAdjustLocks(key)} aria-label={'lock'} size={"xs"}>
                                    {locks[key] ? <FaLock/> : <FaUnlock/>}
                                </Button>
                            </HStack>
                        ))
                    }
                </HStack>
            </Flex>
        </Box>
    )
}