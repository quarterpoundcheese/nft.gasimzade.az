import {
    Box,
    Button,
    FormControl,
    FormLabel,
    HStack,
    Input, Menu,
    MenuButton, MenuItem, MenuList,
    Stack,
    Textarea,
    useToast
} from "@chakra-ui/react";
import React, {forwardRef, useCallback, useImperativeHandle, useMemo, useState} from "react";
import SingleComponent from "./SingleComponent";
import {ComponentState} from "../types/propTypes";
import {generateNumberOfCombs, parseParserText, parseTxt} from "../utils";
import {checkParserText} from "../utils";
// @ts-ignore
import saveAs from 'save-as';
// @ts-ignore
import json2csv from "json2csv";

interface ComponentHolderHandle {
    add: (name: string) => void,
    clear: () => void,
    export: () => ComponentState[],
    import: (parsed: ComponentState[]) => void,
}

enum FileType {
    JSON,
    CSV,
    TXT
}

const ComponentHolder: React.ForwardRefRenderFunction<ComponentHolderHandle, any> = ((props, ref) => {
    const [components, setComponents] = useState<ComponentState[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [numberOfItemsToGenerate, setNumberOfItemsToGenerate] = useState<string | undefined>();
    const [parserText, setParserText] = useState<string>("")
    const toast = useToast();
    const possibleGeneration = useMemo(() => {
        if (components.length === 0) return 0;
        return components.reduce((prev, curr) => prev * curr.count, 1)
    }, [components])


    useImperativeHandle(ref, () => (
        {
            add: (name: string) => {
                setComponents([...components, {
                    name,
                    count: 0,
                    weights: []
                }])
            },
            clear: () => {
                setComponents([]);
                setNumberOfItemsToGenerate("");
            },
            export: () => {
                return components;
            },
            import: (components: ComponentState[]) => {
                setComponents(components)
            }
        }
    ))

    const validateParser = useCallback(() => {
        const error = checkParserText(parserText, components);
        if (error) {
            toast.closeAll();

            return toast({
                title: error,
                status: "error",
            })
        }
        toast.closeAll();

        return toast({
            title: "No errors found!",
            status: "success",
        })

    }, [toast, parserText, components])

    const generateList = useCallback((type: FileType) => {
        const count = possibleGeneration;

        if (parserText !== "") {
            validateParser();
        }

        if (!numberOfItemsToGenerate || numberOfItemsToGenerate === "" || Number.parseInt(numberOfItemsToGenerate) === 0) {
            toast.closeAll();

            return toast({
                title: "Please enter the number of combinations to generate",
                status: "error"
            })
        }

        if (components.length === 0) {
            toast.closeAll();

            return toast({
                title: "Please enter at least 1 component",
                status: "error"
            })
        }

        if (count === 0) {
            toast.closeAll();

            return toast({
                title: "Please enter at least 1 weight",
                status: "error"

            })
        }

        setIsLoading(true);

        let parsed;

        if (parserText !== "") {
            parsed = parseParserText(parserText, components)
        }

        generateNumberOfCombs(Number.parseInt(numberOfItemsToGenerate), components, parsed).then((res) => {
            let data: string;
            let ext: string;
            let mimeType: string;

            switch (type) {
                case FileType.JSON:
                    data = JSON.stringify(res);
                    mimeType = "application/json";
                    ext = "json";
                    break;
                case FileType.CSV:
                    data = json2csv.parse(res);
                    mimeType = "text/csv";
                    ext = "csv";
                    break;
                case FileType.TXT:
                    data = parseTxt(res);
                    mimeType = "text/plain";
                    ext = "txt";
                    break;
                default:
                    data = "";
                    mimeType = "";
                    ext = "";
            }

            const blob = new Blob([data], {type: mimeType});
            saveAs(blob, 'nft-generation.' + ext)
        }).finally(() => setIsLoading(false));
    }, [possibleGeneration, parserText, numberOfItemsToGenerate, components, validateParser, toast]);


    return (
        <Box>
            <Box minHeight={"65vh"} overflowY={"auto"}>
                <Stack spacing={3}>
                    {
                        components.map((comp, key) => (
                            <SingleComponent item={comp} onChange={(c) => {
                                components[key] = c;
                                setComponents([...components])
                            }} key={key}/>
                        ))
                    }
                </Stack>

            </Box>
            <Box>
                <FormControl>
                    <FormLabel>
                        <HStack>
                            <FormLabel>Exclusion Parser</FormLabel>
                            <Button colorScheme={"red"} onClick={() => setParserText("")} size={"xs"}>Clear
                                Parser</Button>
                            <Button colorScheme={"green"} onClick={validateParser} size={"xs"}>Validate</Button>
                        </HStack>
                    </FormLabel>
                    <Textarea placeholder={"Leave empty for no exclusion"} mb={2}
                              onChange={(e) => setParserText(e.target.value)}/>
                </FormControl>
                <HStack>
                    <Input placeholder={"Number of generations"} disabled={isLoading} w={"180px"} size={"xs"}
                           value={numberOfItemsToGenerate}
                           onChange={(e) => {
                               const num = e.target.value;
                               if (Number.parseInt(num) > possibleGeneration) {
                                   return toast(
                                       {
                                           title: "With the components you have selected the maxiumum number is " + possibleGeneration
                                       }
                                   )
                               }
                               setNumberOfItemsToGenerate(num)
                           }}/>
                    <Button isLoading={isLoading} size={"xs"}
                            onClick={() => setNumberOfItemsToGenerate("" + possibleGeneration)}>
                        Max out generations
                    </Button>
                    <Menu>
                        <MenuButton as={Button} isLoading={isLoading} size={"xs"} colorScheme={'green'}>
                            Generate as
                        </MenuButton>
                        <MenuList size={"xs"}>
                            <MenuItem onClick={() => generateList(FileType.JSON)}>JSON</MenuItem>
                            <MenuItem onClick={() => generateList(FileType.CSV)}>CSV</MenuItem>
                            <MenuItem onClick={() => generateList(FileType.TXT)}>TXT</MenuItem>
                        </MenuList>
                    </Menu>
                </HStack>
            </Box>
        </Box>
    )
})

export default forwardRef(ComponentHolder);