import {Box, Button, Grid, Input, useToast} from "@chakra-ui/react";
import React, {useCallback, useRef, useState} from "react";
import ComponentHolder from "./ComponentHolder";
import {FaFileExport, FaFileImport} from "react-icons/fa";
// @ts-ignore
import saveAs from 'save-as';

export default function RandomizerConfig() {
    const [inputValue, setInputValue] = useState("");
    const toast = useToast();
    type ComponentHolderHandle = React.ElementRef<typeof ComponentHolder>;
    const componentHolderRef = useRef<ComponentHolderHandle>(null);
    const inputRef = useRef<HTMLInputElement>(null);
    const inputFile = useRef<HTMLInputElement>(null);

    const onFileInput = useCallback(async (e: React.ChangeEvent<HTMLInputElement>) => {

        console.log(e);

        if (e.target.files?.length === 1) {
            const f = e.target.files[0];

            if(f.type !== "application/json") return toast({
                title: "File must be json"
            })
            
            const currentConfiguration = componentHolderRef.current?.export();
            let ok = true;
            if (currentConfiguration && currentConfiguration.length > 0) {
                ok = window.confirm("This will override your current configuration");
            }

            if (!ok) {
                return
            }
            const buff = await f.arrayBuffer()
            const dec = new TextDecoder("utf-8");

            let parsed;

            try {
                const preJS = dec.decode(buff);
                parsed = JSON.parse(preJS);
            } catch (e: any) {
                toast.closeAll();

                return toast({
                    title: e.message,
                    status: "error"
                })
            }

            componentHolderRef.current?.import(parsed);
            toast.closeAll();

            return toast({
                title: "Successfully loaded",
                status: "success"
            })

        }
    }, [toast])
    
    const onExport = useCallback(() => {
        const output = componentHolderRef.current?.export();
        if(output && output.length === 0) {
            return toast({
                title: "You don't have any components",
                status: "error"
            })
        }

        const blob = new Blob([JSON.stringify(output)], {type: "application/json"});
        saveAs(blob, 'component-save.json')

    }, [toast])

    const submit = useCallback(() => {
        if (inputValue !== "") {
            componentHolderRef.current?.add(inputValue);
            setInputValue("");
            inputRef.current?.focus();
        }

    }, [inputValue])

    return (
        <Box px={3} mt={2}>
            <input type='file' id='file' accept="application/json" ref={inputFile} onChange={onFileInput}
                   style={{display: 'none'}}/>
            <Grid gridTemplateColumns={"3fr 1fr 1fr 1fr 1fr"} gridGap={2}>
                <Input size={"md"} onKeyDown={(e) => {
                    if (e.key === 'Enter') {
                        submit();
                    }
                }} ref={inputRef} onChange={(e) => setInputValue(e.target.value)} value={inputValue}
                       placeholder={"Please enter component name"}/>
                <Button size={"md"} colorScheme={"green"} onClick={submit}>
                    Add
                </Button>
                <Button size={"md"} colorScheme={"red"} onClick={() => componentHolderRef.current?.clear()}>
                    Clear
                </Button>
                <Button size={"md"} onClick={() => inputFile.current?.click()} rightIcon={<FaFileImport/>}
                        colorScheme={"yellow"}>
                    Import
                </Button>
                <Button size={"md"} rightIcon={<FaFileExport/>} onClick={onExport} colorScheme={"yellow"}>
                    Export
                </Button>
            </Grid>
            <Box mt={2}>
                <ComponentHolder ref={componentHolderRef}/>
            </Box>
        </Box>
    )
}