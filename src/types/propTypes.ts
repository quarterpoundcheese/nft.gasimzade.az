export interface ComponentState {
    name: string,
    count: number,
    weights: number[]
}

export interface SingleComponentState{
    onChange: (comp: ComponentState) => void;
}