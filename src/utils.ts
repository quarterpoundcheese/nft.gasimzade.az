import {ComponentState} from "./types/propTypes";

interface IParsedElement {
    name: string,
    index: number,
}

export function weightedRand(weights: number[]): number {
    let sum_of_weight = weights.reduce((previousValue, curr) => previousValue + curr, 0);

    let rnd = Math.random() * sum_of_weight;
    for (let i = 0; i < weights.length; i++) {
        if (rnd < weights[i])
            return i;
        rnd -= weights[i];
    }


    let combs: number[] = []
    weights.forEach((f, key) => {
        let t = new Array(Math.floor(f)).fill(key);
        combs = [...t, ...combs]
    })
    return combs[Math.floor(Math.random() * combs.length)]
}

export function checkParserText(parserText: string, combinations: ComponentState[]): string | undefined {
    const parserTextSplit = parserText.split(",");
    const parserComponents = parserTextSplit.map((t) => t.split("x"));

    for (let i = 0; i < parserComponents.length; i++) {
        for (let j = 0; j < parserComponents[i].length; j++) {
            const componentSplit = parserComponents[i][j].split(":");
            if (componentSplit.length !== 2) return "Parse error expected number where : (Check syntax)";
            const nameMatch = combinations.find((f) => f.name === componentSplit[0])
            if (!nameMatch) return `${componentSplit[0]} is not in component list`;
            if (nameMatch.weights.length < Number.parseInt(componentSplit[1])) return `${componentSplit[0]} has only ${nameMatch.weights.length} but ${componentSplit[1]} was given as input`;
        }
    }
}

export function parseTxt(res: any[]): string {
    let s: string = ""

    const descriptionObj = res[0];
    const descObjKeys = Object.keys(descriptionObj);
    const foundRows: number[][] = [[]];

    for (let i = 0; i < res.length; i++) {
        for (let j = 0; j < descObjKeys.length; j++) {
            if (foundRows[j]) {
                foundRows[j].push(res[i][descObjKeys[j]])
            } else {
                foundRows[j] = [res[i][descObjKeys[j]]];
            }
        }
    }

    for (let i = 0; i < foundRows.length; i++) {
        for (let j = 0; j < foundRows[i].length; j++) {
            s += foundRows[i][j] + (j === foundRows[i].length - 1 ? "" : ", ")
        }
        s += "\n\r"
    }

    console.log(s)

    return s;
}

export function parseParserText(parserText: string, combinations: ComponentState[]) {
    const errors = checkParserText(parserText, combinations);
    if (errors) {
        throw new Error(errors);
    }

    const parserTextSplit = parserText.split(",");
    const parserComponents = parserTextSplit.map((t) => t.split("x"));
    const returningComponents: [IParsedElement, IParsedElement][] = [];

    for (let i = 0; i < parserComponents.length; i++) {
        const parsedEl0: IParsedElement = {
            name: parserComponents[i][0].split(":")[0],
            index: Number.parseInt(parserComponents[i][0].split(":")[1]),
        };
        const parsedEl1: IParsedElement = {
            name: parserComponents[i][1].split(":")[0],
            index: Number.parseInt(parserComponents[i][1].split(":")[1]),
        };

        returningComponents.push([parsedEl0, parsedEl1])
    }

    return returningComponents;

}

function removeExcluded(combs: any, exclusions: [IParsedElement, IParsedElement][]): any[] {
    const newCombs = [];

    for (let i = 0; i < combs.length; i++) {
        if (checkExclusionAgainstGenerated(combs[i], exclusions)) {
            newCombs.push(combs[i])
        }
    }

    return newCombs;
}

function checkExclusionAgainstGenerated(gen: any, exclusions?: [IParsedElement, IParsedElement][]): boolean {
    if (!exclusions) return true;
    for (let i = 0; i < exclusions.length; i++) {
        if (gen[exclusions[i][0].name] === exclusions[i][0].index && gen[exclusions[i][1].name] === exclusions[i][1].index) {
            return false;
        }
    }
    return true;
}

export async function generateNumberOfCombs(combs: number, combinations: ComponentState[], exclusions?: [IParsedElement, IParsedElement][]): Promise<any[]> {

    return new Promise<any>((resolve) => {
        const c: string[] = [];
        const generated: any = [];
        for (let i = 0; i < combs; i++) {
            let toBeHashed = "";
            let singleNft: any = {};
            let count = 0;

            (
                function generation() {
                    count++
                    for (let j = 0; j < combinations.length; j++) {
                        const t = weightedRand(combinations[j].weights) + 1;
                        toBeHashed += t + ",";
                        singleNft[combinations[j].name] = t;
                    }

                    console.log(generated, toBeHashed);

                    if (!c.includes(toBeHashed) && checkExclusionAgainstGenerated(singleNft, exclusions)) {
                        c.push(toBeHashed)
                        generated.push(singleNft);
                        singleNft = {};
                        toBeHashed = "";
                        return
                    }


                    toBeHashed = "";
                    if (count < 500) {
                        generation();
                    }
                }
            )()
        }
        if (exclusions) {
            resolve(removeExcluded(generated, exclusions))
        } else {
            resolve(generated);
        }
    })
}