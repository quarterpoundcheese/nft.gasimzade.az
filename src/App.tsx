import {Box, Heading} from '@chakra-ui/react';
import React from 'react';
import RandomizerConfig from "./components/RanomzierConfig";

function App() {
  return (
      <Box>
          <Heading textAlign={'center'}>
              NFT Randomizer
          </Heading>
          <RandomizerConfig />
      </Box>
  );
}

export default App;
